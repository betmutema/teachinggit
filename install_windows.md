# Installing tools for using git on Windows:
The following will install both a terminal (bash) and Git:
1. Download the Git for Windows [installer](https://gitforwindows.org/).
2. Run the installer and follow the steps below **taking note of each configuration**:
    * Click on "Next" **four** times (two times if you've previously installed Git). You don't need to change anything in the Information, location, components, and start menu screens.
    * From the dropdown menu select **"Use the Nano editor by default"** (NOTE: you will need to scroll up to find it) and click on "Next".
    * On the page that says "Adjusting the name of the initial branch in new repositories".  If you leave this as default the primary project branch will be call 'master'.  If you select a non-default just note that these lessons use the term 'master' for the primary branch.
    * Ensure that "Git from the command line and also from 3rd-party software" is selected and click on "Next". (If you don't do this Git Bash will not work properly, requiring you to remove the Git Bash installation, re-run the installer and to select the "Git from the command line and also from 3rd-party software" option.)
    * Ensure that "Use the native Windows Secure Channel Library" is selected and click on "Next".
    * Ensure that "Checkout Windows-style, commit Unix-style line endings" is selected and click on "Next".
    * Ensure that "Use Windows' default console window" is selected and click on "Next".
    * Ensure that "Default (fast-forward or merge) is selected and click "Next"
    * Ensure that "Git Credential Manager Core" is selected and click on "Next".
    * Ensure that "Enable file system caching" is selected and click on "Next".
    * Click on "Install".
    * Click on "Finish" or "Next".
3. If your "HOME" environment variable is not set (or you don't know what this is): 
    1. Open command prompt (Open Start Menu then type cmd and press Enter)
    2. Type the following line into the command prompt window exactly as shown: setx HOME "%USERPROFILE%" Press Enter, you should see SUCCESS: Specified value was saved.
4. Test the install by typing at the prompt
    git --version 
    Press enter and you should see something like:
    git version 2.31.1.windows.1
5. If you reach the end of 4. you've sucessfully installed git. Quit command prompt by typing exit then pressing Enter
6. At the 'Start' menue search for git and 3 options should now appear: Git CMD, Git GUI, and Git Bash.  Step 3 & 4 above used git cmd but to most easily use Git in Windows open the Git Bash application.  You will now have a easy to use terminal in which you can follow all the following lesson commands.  

Optional video tutorial [here](https://youtu.be/339AEqk9c-8)

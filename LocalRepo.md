# Making a repo locally
- Create a folder 
- Change into it
- Initialise a git repository and note that initialising a repo creates a folder (.git) which is where all version tracking information will be stored.  If you delete that folder you'll loose all version tracking information.
- Get the status of the git repository

```bash
$ mkdir gitdemo
$ ls
$ cd gitdemo
$ git init
$ ls -a
$ ls .git/
$ git status
```



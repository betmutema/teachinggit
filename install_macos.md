# Installing tools for using git on MacOS:

## Terminal (bash):
The default shell in some versions of macOS is Bash, and Bash is available in all versions, so no need to install anything. You access Bash from the Terminal (found in /Applications/Utilities). See the Git installation video tutorial for an example on how to open the Terminal.

To see if your default shell is Bash type echo $SHELL in Terminal and press the Return key. If the message printed does not end with '/bash' then your default is something else and you can run Bash by typing bash

If you want to change your default shell, see this Apple Support article and follow the instructions on ["How to change your default shell"](https://support.apple.com/en-au/HT208050).

## Git:
1. Download and running the most recent installer from [here](https://sourceforge.net/projects/git-osx-installer/files/).
    * Apple maintains and ship their own fork of Git, but it lags mainstream Git significantly so it is recommended that you install from sourceforge rather.
2. Because this installer is not signed by the developer, you may have to right click (control click) on the .pkg file, click Open, and click Open on the pop up window. After installing Git, there will not be anything in your /Applications folder, as Git is a command line program. For older versions of OS X (10.5-10.8) use the most recent available installer labelled "snow-leopard" [available here](http://sourceforge.net/projects/git-osx-installer/files/).


